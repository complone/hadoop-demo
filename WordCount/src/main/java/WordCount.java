import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import java.io.IOException;
import java.util.StringTokenizer;

/**
 * Created by 26645 on 2017/9/10.
 */
public class WordCount {
    public static  class TokenMapper extends Mapper<Object,Text,Text,IntWritable>{
        private  final  static IntWritable one = new IntWritable(1);
        Text Word = new Text();

        //获取输入数据并将数据单元装入mapper
        private void Map(Object key,Text value,Context context) throws IOException,InterruptedException{
            StringTokenizer itr = new StringTokenizer(value.toString());
            while(itr.hasMoreTokens()){
                Word.set(itr.nextToken());
                context.write(Word,one);
            }

        }
    }

    //产品能够mapper的全部输出中给出最终结果
    public static  class  IterCountReducer extends Reducer<Object,Text,Text,IntWritable>{
        private  final  static IntWritable result = new IntWritable();

        public void Reduce(Text key,Iterable<IntWritable> values ,Context context) throws IOException,InterruptedException{
            int Sum = 0;
            for(IntWritable val: values){
                Sum += val.get();

            }
            result.set(Sum);
            context.write(key,result);
        }


    }

    public static void main(String args[]) throws  Exception{

        //Create a new job
        Configuration conf = new Configuration();
        Job job =new Job(conf,"WordCount");
        //job-specific paramsters
        job.setJobName("Word Count");
        job.setMapperClass(TokenMapper.class);
        job.setCombinerClass(IterCountReducer.class);
        job.setJarByClass(WordCount.class);
        job.setReducerClass(IterCountReducer.class);
        //设置Mapper输入<key,value>键值对
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);
        FileInputFormat.addInputPath(job,new Path(args[0]));
        FileOutputFormat.setOutputPath(job,new Path(args[1]));
        try
        {
            System.exit(job.waitForCompletion(true)?0:1);
        }
        catch (Exception e){
            e.printStackTrace();
        }


    }

}
